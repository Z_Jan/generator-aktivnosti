<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ActivitiesController;
use App\Http\Controllers\ProposedActivitiesController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\FeedbacksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('webpage.landing');
});
Route::get('/oddaj-idejo', function () {
    return view('webpage.landing');
});

Route::post('/get-random-activity', [ActivitiesController::class, "randomActivity"]);
Route::post('/like-activity', [ActivitiesController::class, "likeActivity"]);
Route::post('/skip-activity', [ActivitiesController::class, "skipActivity"]);
Route::post('/post-feedback', [FeedbacksController::class, "saveFeedback"]);
Route::get('/get-feedbacks', [FeedbacksController::class, "getFeedbacks"]);

Route::get('/test', [ActivitiesController::class, "test"]);

Route::apiResource('categories', CategoriesController::class)->only(['index']);
Route::apiResource('activities', ActivitiesController::class)->only(['index']);
Route::apiResource('proposedActivities', ProposedActivitiesController::class)->only(['store']);

// Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::prefix('admin')->group(function () {
        Route::view('/', 'admin.dashboard');
        Route::view('/dejavnosti', 'admin.dashboard');
        Route::view('/dejavnosti/dodaj', 'admin.dashboard');
        Route::view('/dejavnosti/predlogi', 'admin.dashboard');
        Route::view('/kategorije', 'admin.dashboard');
        Route::view('/kategorije/dodaj', 'admin.dashboard');
        Route::view('/uporabniki', 'admin.dashboard');
        Route::view('/mnenja', 'admin.dashboard');
    });

    Route::apiResource('activities', ActivitiesController::class)->except(['index']);
    Route::apiResource('proposedActivities', ProposedActivitiesController::class)->except(['store']);
    Route::post('/approve-activity', [ProposedActivitiesController::class, "approve"]);
    Route::apiResource('categories', CategoriesController::class)->except(['index']);

    // analytics
    Route::get('/get-generates-count', [AnalyticsController::class, "getCount"]);
    Route::post('/get-generates-count-time-range', [AnalyticsController::class, "getTimeRangeCount"]);
    Route::get('/get-most-liked', [AnalyticsController::class, "getMostLiked"]);
    Route::get('/get-most-skipped', [AnalyticsController::class, "getMostSkipped"]);
});

Route::middleware(['auth:api'])->group(function () {
    // Route::apiResource('activities', ActivitiesController::class);
});


Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);

// Password Reset Routes...
/* $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset'); */