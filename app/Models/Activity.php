<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $dateFormat = 'Y-m-d H:i:s';

    protected $fillable = [
        'name', 'daytime', 'outdoor', 'likes', 'times_skipped'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function feedbacks() {
        return $this->hasMany(Feedback::class);
    }
}
