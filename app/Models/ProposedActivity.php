<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposedActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'daytime', 'outdoor'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
