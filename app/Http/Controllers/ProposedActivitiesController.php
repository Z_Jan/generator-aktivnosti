<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\ProposedActivity;
use App\Models\Category;

class ProposedActivitiesController extends Controller
{
    /**
     * Return a listing of the resource.
     */
    public function index()
    {
        $activities = ProposedActivity::with("category")->get();
        return response()->json([
            "data" => $activities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|string',
            'category_id'   => 'required|integer',
            'daytime'       => 'required|boolean',
            'outdoor'       => 'required|boolean',
        ]);

        $category = Category::find($request->category_id);
        $category->proposedActivities()->create([
            'name'      => $request->name,
            'daytime'   => $request->daytime,
            'outdoor'   => $request->outdoor,
        ]);
        
        return response()->json([
            "message" => "Hvala! Uspešno ste oddali svojo idejo."
        ]);
    }

    
    public function approve(Request $request)
    {
        $request->validate([
            'id'            => 'required|integer',
            'name'          => 'required|string',
            'category_id'   => 'required|integer',
            'daytime'       => 'required|boolean',
            'outdoor'       => 'required|boolean',
        ]);

        $category = Category::find($request->category_id);
        $category->activities()->create([
            'name'      => $request->name,
            'daytime'   => $request->daytime,
            'outdoor'   => $request->outdoor,
        ]);

        $proposedActivity = ProposedActivity::find($request->id);
        $proposedActivity->delete();
        
        return response()->json([
            "message" => "Activity approved."
        ]);
    }

    /**
     * Return the specified resource.
     */
    public function show($id)
    {
        $activity = ProposedActivity::find($id);
        return response()->json([
            "data" => $activity
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ProposedActivity $proposedActivity)
    {
        $request->validate([
            'name'          => 'required|string',
            'category_id'   => 'required|integer',
            'daytime'       => 'required|boolean',
            'outdoor'       => 'required|boolean',
        ]);

        $proposedActivity->name = $request->name;
        $proposedActivity->daytime = $request->daytime;
        $proposedActivity->outdoor = $request->outdoor;
        
        if ($proposedActivity->category->id != $request->category_id) {
            $category = Category::find($request->category_id);
            $proposedActivity->category()->associate($category);
        }

        $proposedActivity->save();

        return response()->json([
            "message" => "ProposedActivity has been updated."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProposedActivity $activity)
    {
        $activity->delete();
        return response()->json([
            "message"   => "ProposedActivity has been deleted.",
            "data"      => $activity
        ]);
    }
}
