<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Category;
use App\Models\GenerateCount;
use App\Models\Feedback;

class ActivitiesController extends Controller
{
    /**
     * Return a listing of the resource.
     */
    public function index()
    {
        $activities = Activity::with("category")->get();
        return response()->json([
            "data" => $activities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|string',
            'category_id'   => 'required|integer',
            'daytime'       => 'required|boolean',
            'outdoor'       => 'required|boolean'
        ]);

        $category = Category::find($request->category_id);
        $category->activities()->create([
            'name'      => $request->name,
            'daytime'   => $request->daytime,
            'outdoor'   => $request->outdoor
        ]);

        return response()->json([
            "message" => "Aktivnost dodana."
        ]);
    }

    /**
     * Return the specified resource.
     */
    public function show($id)
    {
        $activity = Activity::find($id);
        return response()->json([
            "data" => $activity
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Activity $activity)
    {
        $request->validate([
            'name'          => 'required|string',
            'category_id'   => 'required|integer',
            'daytime'       => 'required|boolean',
            'outdoor'       => 'required|boolean'
        ]);

        $activity->name = $request->name;
        $activity->daytime = $request->daytime;
        $activity->outdoor = $request->outdoor;
        
        if ($activity->category->id != $request->category_id) {
            $category = Category::find($request->category_id);
            $activity->category()->associate($category);
        }

        $activity->save();

        return response()->json([
            "message" => "Aktivnost posodobljena."
        ]);
    }

    public function likeActivity(Request $request)
    {
        $request->validate([
            "id"   => "required|exists:activities"
        ]);
        
        $activity = Activity::find($request->id);
        $activity->increment("likes");

        return response()->json([
            "message"   => "Všečkano."
        ]);
    }

    public function skipActivity(Request $request)
    {
        $request->validate([
            "id"   => "required|exists:activities"
        ]);
        
        $activity = Activity::find($request->id);
        $activity->increment("times_skipped");

        return response()->json([
            "message"   => "Preskočeno."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Activity $activity)
    {
        $activity->delete();
        return response()->json([
            "message"   => "Aktivnost odstranjena.",
            "data"      => $activity
        ]);
    }

    public function randomActivity(Request $request)
    {
        $request->validate([
            "previous"  => 'exists:activities,id|nullable',
            "daytime"   => 'boolean|nullable',
            "outdoor"   => 'boolean|nullable',
            "time"      => 'integer|nullable',
        ]);

        $conditions = [];
        $data = null;

        // prevent activity being shown again
        if (isset($request->previous)) {
            array_push($conditions, array("id", "!=", $request->previous));
        }

        if (isset($request->daytime)) {
            array_push($conditions, array("daytime", "=", $request->daytime));
        }
        
        if (isset($request->outdoor)) {
            // user's filter preference has priority over weather check
            array_push($conditions, array("outdoor", "=", $request->outdoor));
        } else {
            // check raining if user didn't choose outdoor filter
            if (isset($request->time)) { // if time is chosen, pass it to isItRaining function
                if ($this->isItRaining(request()->ip(), $request->time)) {
                    array_push($conditions, array("outdoor", "=", 0));
                }
            } else { // otherwise check current weather
                if ($this->isItRaining(request()->ip(), 0)) {
                    array_push($conditions, array("outdoor", "=", 0));
                }
            }
        }

        if (isset($request->category)) {
            if ($c = Category::with('activities')->find($request->category)) {
                $data = $c->activities()->where($conditions)->get();

                // if selected category is tourism, fetch attractions from openTripMap API and add them to collection
                if ($c->name === "Turizem") {
                    
                    // if ($attractions = $this->getAttractions("93.103.159.191")->features) {
                    $res = $this->getAttractions(request()->ip());
                    if (isset($res->features)) {
                        $attractions = $res->features;
                        foreach ($attractions as $a) {
                            if ($name = $a->properties->name) {
                                $obj = (object) ['id' => null, 'name' => "Obišči atrakcijo: ${name}"];
                                $data->push($obj);
                            }
                        }
                    }
                }

                $data = $data->random(1);
            }
        } else {
            $data = Activity::where($conditions)->get()->random(1);
        }

        GenerateCount::create();

        if (!isset($data)) {
            return response()->json([
                "status"    => "not_found",
                "msg"       => "Za nastavljene filtre ni dejavnosti."
            ]);
        }

        return response()->json([
            "status"    => "success",
            "data"      => $data
        ]);
    }

    public function test(Request $request) {
        // return $this->isItRaining(request()->ip());
        return response()->json([
            "msg" => $this->isItRaining(request()->ip(), 0)
        ]);
        // return $this->isItRaining("46.123.226.239", 0);
    }


    private function isItRaining($ip, $time) {
        // get user location (langtitude and longitude)
        if (!$data = \Location::get($ip)) return false;
        $lat = $data->latitude;
        $lng = $data->longitude;

        $apiKey = config("app.openWeatherMap_apiKey");
        $url = "https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&exclude=minutely,current,daily,alerts&appid=${apiKey}";

        $res = $this->sendGuzzleRequest($url);
        
        if (isset($res->hourly[$time])) {
            if ($weather = $res->hourly[$time]->weather[0]->main) {
                if ($weather === "Clouds" || $weather === "Thunderstorm") {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private function getAttractions($ip) {
        // get user location (langtitude and longitude)
        if (!$data = \Location::get($ip)) return [];
        $lat = $data->latitude;
        $lng = $data->longitude;

        $apiKey = config("app.openTripMap_apiKey");

        $radius = "1000";

        $url = "https://api.opentripmap.com/0.1/en/places/radius?apikey=${apiKey}&radius=${radius}&lon=${lng}&lat=${lat}";

        if ($res = $this->sendGuzzleRequest($url)) {
            return $res;
        }

        return [];
    }

    private function sendGuzzleRequest($url) {
        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);

        if ($res->getStatusCode() == 200) {
            return json_decode($res->getBody());
        }

        return null;
    }
}
