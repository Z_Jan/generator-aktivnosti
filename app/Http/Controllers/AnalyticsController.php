<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Category;
use App\Models\GenerateCount;
use Carbon\Carbon;

class AnalyticsController extends Controller
{
    public function getCount() {
        return response()->json([
            "count" => GenerateCount::count()
        ]);
    }

    public function getTimeRangeCount(Request $request) {
        $request->validate([
            "from_date" => 'date|required',
        ]);

        $date = Carbon::createFromFormat('d-m-Y', $request->from_date);

        $data = GenerateCount::whereDate('created_at', '>=', $date)->count();

        return response()->json([
            "count" => $data
        ]);
    }

    public function getMostLiked() {
        return response()->json([
            "data" => Activity::orderBy("likes", "desc")->limit(5)->get()
        ]);
    }

    public function getMostSkipped() {
        return response()->json([
            "data" => Activity::orderBy("times_skipped", "desc")->limit(5)->get()
        ]);
    }

}
