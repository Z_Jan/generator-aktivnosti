<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Models\Activity;

class FeedbacksController extends Controller
{
    public function getFeedbacks() {
        $feedbacks = Feedback::with("activity")->get();
        
        return response()->json([
            "feedbacks" => $feedbacks
        ]);
    }

    public function saveFeedback(Request $request) {
        $request->validate([
            "activity_id"   => 'required|exists:activities,id',
            "message"       => 'required|string',
        ]);

        $activity = Activity::find($request->activity_id);
        $activity->feedbacks()->create([
            'message'   => $request->message
        ]);

        return response()->json([
            "msg"   => "success"
        ]);
    }
}
