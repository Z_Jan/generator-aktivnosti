<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Generator dejavnosti</title>

    <script src="{{ asset('js/app.js') }}?v=1.2" defer></script>

    <link href="{{ asset('css/webpage.css') }}?v=1.2" rel="stylesheet">
</head>
<body>
    <div id="app">

        <main>
            @yield('content')
        </main>

    </div>
</body>
</html>
