<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin</title>

    <script src="{{ asset('js/app.js') }}?v=1.21" defer></script>

    <link href="{{ asset('css/admin.css') }}?v=1.21" rel="stylesheet">
</head>
<body>
    <div id="app">

        <main>
            @yield('content')
        </main>

        @auth
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        @endauth
    </div>
</body>
</html>
