require('./bootstrap');
window.Vue = require('vue').default;

import router from "./router";

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.component('adminDashboard', require('./layouts/Admin.vue').default);

Vue.component('webpageMain', require('./layouts/WebpageMain.vue').default);

const app = new Vue({
    router,
    el: '#app',
});