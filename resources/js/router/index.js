import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// webpage
import LandingPage from "../views/webpage/LandingPage.vue";
import ProposeActivityPage from "../views/webpage/ProposeActivityPage.vue";

// admin
import dashboard from "../views/admin/Dashboard.vue";
import listActivities from "../views/admin/ListActivities.vue";
import addActivity from "../views/admin/AddActivity.vue";
import proposedActivities from "../views/admin/ProposedActivities.vue";
import listCategories from "../views/admin/ListCategories.vue";
import addCategory from "../views/admin/AddCategory.vue";
import feedbackMessages from "../views/admin/feedbackMessages.vue";

const routes = [
    // webpage routes
    {
        path: "/",
        name: "landing-page",
        component: LandingPage,
    },
    {
        path: "/oddaj-idejo",
        name: "oddaj-idejo",
        component: ProposeActivityPage,
    },
    // admin routes
    {
        path: "/admin",
        name: "dashboard",
        component: dashboard,
    },
    {
        path: "/admin/dejavnosti",
        name: "dejavnosti",
        component: listActivities,
    },
    {
        path: "/admin/dejavnosti/dodaj",
        name: "dejavnosti-dodaj",
        component: addActivity,
    },
    {
        path: "/admin/dejavnosti/predlogi",
        name: "dejavnosti-predlogi",
        component: proposedActivities,
    },
    {
        path: "/admin/kategorije",
        name: "kategorije",
        component: listCategories,
    },
    {
        path: "/admin/kategorije/dodaj",
        name: "kategorije-dodaj",
        component: addCategory,
    },
    {
        path: "/admin/mnenja",
        name: "mnenja",
        component: feedbackMessages,
    },

];

const router = new VueRouter({
    mode: "history",
    routes,
});

export default router;