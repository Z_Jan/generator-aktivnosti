<?php

namespace Database\Factories;

use App\Models\ProposedActivity;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProposedActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProposedActivity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => Category::factory(),
            'name' => $this->faker->sentence(4),
            'daytime' => $this->faker->boolean(),
            'outdoor' => $this->faker->boolean()
        ];
    }
}
